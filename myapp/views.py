from .app import app
from flask import render_template, redirect, request
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, SubmitField, PasswordField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask_login import login_user, current_user
from .loaddb import *

class LoginForm(FlaskForm):
    username = StringField("Username")
    password = PasswordField("Password")
    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None



@app.route("/index")
def index():
    return render_template("index.html")

@app.route("/login_template")
def login_template():
    return render_template("login_template.html")

@app.route("/change_password")
def change_password():
    return render_template("change_password.html")

@app.route("/aides/<int:id>")
def aides(id):
    a = get_aide_by_id(id)
    return render_template("aides_template.html", aide=a)

@app.route("/aides_template")
def aides_template():
    return render_template("aides_template.html")

@app.route("/bon_plans")
def bon_plans():
    return render_template("bon_plans.html")

@app.route("/edit_profile_template")
def edit_profile_template():
    return render_template("edit_profile_template.html")

@app.route("/register_template")
def register_template():
    return render_template("register_template.html")

@app.route("/aide_utilisateur")
def aide_utilisateur():
    return render_template("aide_utilisateur.html")

@app.route("/collaboration")
def collaboration():
    return render_template("collaboration.html")

@app.route("/detail_collaboration")
def detail_collaboration():
    return render_template("detail_collaboration.html")

@app.route("/users")
def users():
    return render_template("users.html")

@app.route("/infos_user_template/<int:id>")
def infos_user_template(id):
    # a = get_aide_by_id(id)
    return render_template("infos_user_template.html", user=u)

@app.route("/user_detail")
def user_detail():
    return render_template("user_detail.html")